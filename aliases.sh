
# git
alias gco='git checkout'
alias gl='git pull --prune'
alias gca='git add -A ; git commit -m '
alias gp='git push origin HEAD'
# Delete remote tracking Git branches where the upstream branch was deleted
alias git_prune="git fetch --prune && git branch -vv | grep 'origin/.*: gone]' | awk '{print \$1}' | xargs git branch -d"

# kubernetes
alias kk=kubectl

# docker
export DOCKER_HOST=localhost:2375
#INSTANCE=primary; export DOCKER_HOST="$(multipass info $INSTANCE | awk '/IPv4:/ {print $2}'):2375"

# shell 
set -o vi
alias lsd="ls -al | awk '/^d/ {print $4}'"
#NEWLINE=$'\n'
#export PS1=${NEWLINE}$PS1


