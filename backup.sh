TARGET=~/dotfiles
SOURCE=~

FILES="
.gitconfig
.zshrc
"

for args in $FILES
do
  rsync -av $SOURCE/$args $TARGET/
done

brew bundle dump 

