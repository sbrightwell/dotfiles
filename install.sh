
brew bundle install --file Brewfile

code -v > /dev/null 2>&1
if [[ $? -eq 0 ]];then
	code --install-extension davidanson.vscode-markdownlint
	code --install-extension eamodio.gitlens
	code --install-extension esbenp.prettier-vscode
	code --install-extension gitlab.gitlab-workflow
	code --install-extension ms-python.python
	code --install-extension ms-python.vscode-pylance
	code --install-extension ms-vscode-remote.remote-ssh
	code --install-extension ms-vscode-remote.remote-ssh-edit
	code --install-extension redhat.vscode-yaml
	code --install-extension tomoki1207.pdf
	code --install-extension visualstudioexptteam.vscodeintellicode
	code --install-extension vscjava.vscode-java-pack
	code --install-extension vscode-icons-team.vscode-icons
	code --install-extension vscodevim.vim
else
	echo "Can't find code cli to install extensions"
fi
